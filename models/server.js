const express = require("express");
const http = require("http");
const socketio = require("socket.io");
const path = require("path");
const cors = require("cors");
const Sockets = require("./sockets");

class Server {
  constructor() {
    this.app = express();
    this.port = process.env.PORT || 3000;
    // Http server
    this.server = http.createServer(this.app);
    // Sockets setup
    this.io = socketio(this.server, {
      /* settings */
    });
  }
  configSockets() {
    new Sockets(this.io);
  }

  middlewares() {
    this.app.use(express.static(path.resolve(__dirname, "../public")));
    this.app.use(cors());
  }
  execute() {
    this.middlewares();
    this.configSockets();
    this.server.listen(this.port, () => {
      console.log("Connected on port", this.port);
    });
  }
}

module.exports = Server;
